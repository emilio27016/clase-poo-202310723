﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace practica_2
{
    class Cliente
    {
        public string ApellidoMaterno = "";
        public string ApellidoPaterno = "";
        public int CP = 0;
        public string Dirección = "";
        public int IDCliente = 0;
        public string Nombre = "";
        public int Telefono = 0;
        public int TelefonoCasa = 0;
        public int TelefonoMovil = 0;
    }
}
