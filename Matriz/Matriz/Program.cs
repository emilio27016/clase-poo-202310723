﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matriz
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] Matriz = new int[2, 4];

            for (int m = 0; m < 4; m++)
            {
                for (int n = 0; n < 2; n++)
                {
                    Console.WriteLine("Ingresa un numero");
                    int num = Convert.ToInt32(Console.ReadLine());
                    Matriz[n, m] = num;
                    Console.WriteLine(m + "," + n);
                }
            }

            for (int m = 0; m < 2; m++)
            {
                for (int n = 0; n < 4; n++)
                {
                    Console.WriteLine(Matriz[m, n]);
                }
            }

            Console.ReadKey();

        }
    }
}
