﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 obj = new Class1();
            int resultado = obj.Suma(10, 4);
            Console.WriteLine("Resultado metodo 1: {0}", resultado);
            Console.ReadKey();

            int resultado2 = obj.Suma(15, 35, 23);
            Console.WriteLine("Resultado metodo 2: {0}", resultado2);
            Console.ReadKey();

            int resultado3 = obj.multiplica(10, 12, 12, 18);
            Console.WriteLine("Resultado metodo 3: {0}", resultado3);
            Console.ReadKey();
        }
    }
}
