﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Constructores
{
    class Operaciones
    {
        private int multa;
        public Operaciones(int multa)
        {
            this.multa = multa;
        }


        public int CalcularMulta()
        {
            int resultado = 0;

            switch (this.multa)
            {
                case 0:
                    resultado = 180;
                    break;

                default:
                    resultado = 0;
                    break;

                case 1:
                    resultado = 600;
                    break;

                case 2:
                    resultado = 250;
                    break;

                case 3:
                    resultado = 1200;
                    break;

            }


            return resultado;
        }

    }
}