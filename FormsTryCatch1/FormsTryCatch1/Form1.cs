﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsTryCatch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void resultado_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            try
            {
                double numero1 = 0;
                double numero2 = 0;
                double result = 0;
                numero1 = Convert.ToDouble(num1.Text);
                numero2 = Convert.ToDouble(num2.Text);
                result = numero1 + numero2;
                resultado.Text = Convert.ToString(result);
            }
            catch
            {
                MessageBox.Show("Parece que algo anda mal...", "Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }


        }
    }
}
