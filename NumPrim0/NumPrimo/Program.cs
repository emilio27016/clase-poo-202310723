﻿using System;

namespace NumPrimo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Determinar si un numero es primo ");
            Console.WriteLine("************************************");

            int n = 0;
            int c = 0;

            Console.WriteLine("Ingrese un numero: ");
            n = Convert.ToInt32(Console.ReadLine());
            for ( int i = 1; i < (n+1); i++)
            {

                if (n%i==0)
                {
                    c++;
                }

            }
            if (c!=2)
            {
                Console.WriteLine("no es primo");

            }
            else
            {
                Console.WriteLine("es primo");
            }
            


        }
    }
}
