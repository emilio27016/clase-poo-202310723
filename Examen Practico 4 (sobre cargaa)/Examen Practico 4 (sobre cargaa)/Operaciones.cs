﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Examen_Practico_4__sobre_cargaa_
{
    class Operaciones
    {
        public int Oper(int a, int b, int c, int d, int e, int f)
        {
            int resultado = a * b * c * d / e + f;
            return resultado;
        }

        public double Promedio(double Español, double Matematicas, double Historia, double Ciensas, double Calculo)
        {
            double resultado = Español + Matematicas + Historia + Ciensas + Calculo / 5;
            return resultado;
        }
    }
}
