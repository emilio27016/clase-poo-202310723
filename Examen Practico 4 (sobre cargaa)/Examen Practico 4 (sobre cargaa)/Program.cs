﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Examen_Practico_4__sobre_cargaa_
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones obj = new Operaciones();

            int resultado = obj.Oper(3, 2, 13, 190, 180, 200);
            Console.WriteLine("Resultado metodo 1: {0}", resultado);
            Console.ReadKey();

            double resultado2 = obj.Promedio(7.8, 8.0, 9.0, 6.5, 8.5);
            Console.WriteLine("Resultado metodo 2: {0}", resultado2);
            Console.ReadKey();

        }
    }
}
