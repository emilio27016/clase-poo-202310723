﻿using System;

namespace ArregloMultidimencional
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] Matriz = new int[2,4];

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.WriteLine("Ingresa un numero");
                    int num = Convert.ToInt32(Console.ReadLine());
                    Matriz[j, i] = num;
                    Console.WriteLine(i + "," + j);
                }
            }

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine(Matriz[i, j]);
                }
            }

            Console.ReadKey();
        }
    }
}
