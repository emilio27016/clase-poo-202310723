﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abstracto
{
    class Program
    {
        static void Main(string[] args)
        {
            Avion obj = new Avion();
            Auto obj1 = new Auto();
            obj.Mantenimiento();
            obj1.Mantenimiento();
        }
    }
}
