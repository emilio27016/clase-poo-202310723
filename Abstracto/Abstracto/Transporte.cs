﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abstracto
{
     abstract class Transporte
    {
         public abstract void Mantenimiento();
    }

    class Auto : Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Listado de Mantenimiento de AUTO-*\n-Cambio de aceite\n-Cambio de Bujias\n-Chequeo de Llantas\n-Cambio de filtros");
            Console.ReadKey();
        }
    
    
    }

    class Avion : Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Listado de Mantenimiento de AVION-*\n-Alas\n-Motor\n-Llantas");
            Console.ReadKey();
        }
    }
}
