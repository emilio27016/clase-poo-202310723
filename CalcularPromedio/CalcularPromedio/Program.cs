﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalcularPromedio
{
    class Program
    {
        static void Main(string[] args)
        {
            int cal1, cal2, cal3;
            int PromReal, CalcPromedio;

            Console.WriteLine("Primera calificacion");
            cal1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Segunda calificacion");
            cal2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Tercera calificacion");
            cal3 = Convert.ToInt32(Console.ReadLine());

            PromReal = cal1 + cal2 + cal3;
            CalcPromedio = PromReal / 3;

            Console.WriteLine("El promedio real es: "+ PromReal);
            Console.WriteLine("El CalcPromedio es: "+ CalcPromedio);
            Console.ReadKey();
        }
    }
}
