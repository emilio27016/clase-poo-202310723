﻿namespace Practica_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnClase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnClase
            // 
            this.BtnClase.Location = new System.Drawing.Point(295, 99);
            this.BtnClase.Name = "BtnClase";
            this.BtnClase.Size = new System.Drawing.Size(90, 23);
            this.BtnClase.TabIndex = 0;
            this.BtnClase.Text = "Crear Clase";
            this.BtnClase.UseVisualStyleBackColor = true;
            this.BtnClase.Click += new System.EventHandler(this.BtnClase_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 292);
            this.Controls.Add(this.BtnClase);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnClase;
    }
}

