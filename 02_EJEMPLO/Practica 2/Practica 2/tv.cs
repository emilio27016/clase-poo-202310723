﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practica_2
{
    class tv
    {
        private int tamanio = 0;
        private int volumen = 0;
        private string color = "";
        private int brillo = 0;
        private int contraste = 0;
        private string marca = "";


        public void setTamanio(int tamanio) {
            this.tamanio = tamanio;
        }

        public int getTamanio()
        {
            return this.tamanio;
        }

        public void setVolumen(int volumen)
        {

            this.volumen = volumen;
        }

        public int getVolumen()
        {
            return this.volumen;
        }

        public void setColor(string color)
        {

            this.color = color;
        }

        public string getColor()
        {
            return this.color;
        }

        public void setBrillo(int brillo)
        {
            this.brillo = brillo;
        }

        public int getBrillo()
        {
            return this.brillo;
        }

        public void setContraste(int constraste) 
        {
            this.contraste = constraste;
        }

        public int getContraste()
        {
            return this.contraste;
        }

        public void setMarca( string marca) 
        {
            this.marca = marca;
        }

        public string getMarca()
        {
            return this.marca;
        }







        
    }
}
