﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elevar_Cuadrado
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 obj = new Class1();
            int resultado = obj.Potencia(5);
            Console.WriteLine("Resultado metodo 1: {0}", resultado);
            Console.ReadKey();

            double resultado2 = obj.Potencia(5.888854);
            Console.WriteLine("Resultado metodo 2: {0}", resultado2);
            Console.ReadKey();

            float resultado3 = obj.Potencia(3.9f);
            Console.WriteLine("Resultado metodo 3: {0}", resultado3);
            Console.ReadKey();
        }
    }
}
