﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elevar_Cuadrado
{
    class Class1
    {
        public int Potencia(int numero)
        {
            int resultado = numero * numero;
            return resultado;

        }

        public double Potencia(double numero)
        {
            double resultado = numero * numero;
            return resultado;
        }

        public float Potencia(float numero)
        {
            float resultado = numero * numero;
            return resultado;
        }
    }
}
