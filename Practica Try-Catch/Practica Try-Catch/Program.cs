﻿using System;

namespace Practica_Try_Catch
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arreglo = new int [10];
            try
            {
                while(true)
                {
                    Console.WriteLine("Hola");
                }
            }
            catch(InvalidCastException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.WriteLine("Hola este es el final");
            }
        }
    }
}
