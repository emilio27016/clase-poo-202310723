﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Circunferencia
{
    class Circunferencia
    {
        public double radio = 0;
        public double area = 0;
        public double perimetro = 0;

        public void setRadio(double radio)
        {
            this.radio = radio;
        }

        public double getRadio()
        {
            return this.radio;
        }

        public void setArea (double area)
        {
            this.area = area;
        }

        public double getArea()
        {
            return this.area;
        }

        public void setPerimetro(double perimetro)
        {
            this.perimetro = perimetro;
        }

        public double getPerimetro()
        {
            return this.perimetro;
        }



    }
}
