﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Examen_practico_4__abstract_
{
    abstract class Empleado
    {
        public abstract void Datos();
    }

    class DatosE : Empleado
    {
        public override void Datos()
        {
            Console.WriteLine("*-Datos del empleado-*\n-Nombres: César Emiliano\n-Apellidos: Reyes Pámanes");
            Console.WriteLine("-Numero de control: 202310723\n");
            Console.ReadKey();
        }
    }

    class DatosE2 : Empleado
    {
        public override void Datos()
        {
            Console.WriteLine("*-Datos del segundo Empleado-*\n-Nombres: Victor Hugo\nApellidos: Martinez");
            Console.WriteLine("-Numero de control: 2323152");
            Console.ReadKey();
        }
    }
}
