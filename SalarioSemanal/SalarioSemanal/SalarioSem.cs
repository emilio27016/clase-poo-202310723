﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalarioSemanal
{
    abstract class SalarioSem
    {
        public abstract void DatosSalario();
    }

    class Datos : SalarioSem
    {
        public override void DatosSalario()
        {
            Console.WriteLine("*-Datos del empleado-*\n-Primer Nombre: César\n-Apellido paterno: Reyes");
            Console.WriteLine("Numero de seguro social: 202310723\n");
            Console.ReadKey();

            int SueldoDia, Dias, total;
            Console.WriteLine("Le vamos a ayudar a calcular su sueldo :D\n");
            Console.WriteLine("Ingrese lo que gana por día: ");
            SueldoDia = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese los días que trabaja: ");
            Dias = int.Parse(Console.ReadLine());

            total = SueldoDia * Dias;

            Console.WriteLine("Su sueldo total es de: " + total);
            Console.ReadKey();



            
        }
    }
}
