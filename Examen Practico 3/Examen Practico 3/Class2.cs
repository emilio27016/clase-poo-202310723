﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Class2
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;

        public void metodo1()
        {
            Console.WriteLine("Este es el metodo 1 de herencia");

        }

        private void metodo2()
        {
            Console.WriteLine("Este metodo 2");
        }

        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 de herencia");
        }

        public void AccesoMetodo2()
        {
            metodo2();
        }
    }

    class Hijo : Class2
    {
        public void AccesoMetodo3()
        {
            metodo3();
        }
    }
}
