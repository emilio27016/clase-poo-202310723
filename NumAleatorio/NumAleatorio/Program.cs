﻿using System;

namespace NumAleatorio
{
    class Aleatorio
    {
        static void Main(string[] args)
        {
            Random aleatorio = new Random();

            int numero = aleatorio.Next(0, 250);

            Console.WriteLine("El numero generado es " + numero);
            Console.ReadKey();

        }
    }
}
